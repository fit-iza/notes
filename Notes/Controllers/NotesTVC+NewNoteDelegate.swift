//
//  NotesTVC+NewNoteDelegate.swift
//  Notes
//
//  Created by Filip Klembara on 11/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation

extension NotesTableViewController: NewNoteDelegate {
    func saveNew(note text: String) {
        let note = Note(date: Date(), text: text)
        model.insert(note: note)
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPath], with: .fade)
    }
}
