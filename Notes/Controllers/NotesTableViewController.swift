//
//  NotesTableViewController.swift
//  Notes
//
//  Created by Filip Klembara on 06/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import UIKit

class NotesTableViewController: UITableViewController {

    let model = Model.shared

    private struct Segues {
        private init() { }
        static let newNoteSegue = "NewNoteSegue"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = model

        clearsSelectionOnViewWillAppear = false

        navigationItem.leftBarButtonItem = editButtonItem
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segues.newNoteSegue,
            let vc = segue.destination as? NewNoteViewController else {

                return
        }
        vc.delegate = self
    }
}
