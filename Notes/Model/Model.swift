//
//  Model.swift
//  Notes
//
//  Created by Filip Klembara on 06/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation
import UIKit

public class Model: NSObject {
    private override init() { }

    public static let shared = Model() // singleton

    private var notes = [
        Note(date: Date(), text: "Buy some milk"),
        Note(date: Date() - 3600 * 24 * 3, text: "Buy a Dogo")
    ]

    private var count: Int {
        return notes.count
    }
}

extension Model: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }


    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath)
        let note = notes[indexPath.row]
        cell.detailTextLabel?.text = note.text

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"

        cell.textLabel?.text = dateFormatter.string(from: note.date)

        return cell
    }

    // Override to support conditional editing of the table view.
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }


    // Override to support editing the table view.
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            notes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // nope
        }
    }

    public func insert(note: Note) {
        notes.insert(note, at: notes.startIndex)
    }

    // Override to support rearranging the table view.
    public func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        notes.swapAt(fromIndexPath.row, to.row)
    }

    // Override to support conditional rearranging of the table view.
    public func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }

}
